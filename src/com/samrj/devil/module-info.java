
module devil.util {
    requires jflac.codec;
    requires java.desktop;
    requires org.lwjgl;
    requires org.lwjgl.assimp;
    requires org.lwjgl.glfw;
    requires org.lwjgl.openal;
    requires org.lwjgl.opengl;
    requires org.lwjgl.stb;
    exports com.samrj.devil.al;
    exports com.samrj.devil.game;
    exports com.samrj.devil.game.step;
    exports com.samrj.devil.game.sync;
    exports com.samrj.devil.geo2d;
    exports com.samrj.devil.geo3d;
    exports com.samrj.devil.gl;
    exports com.samrj.devil.graphics;
    exports com.samrj.devil.gui;
    exports com.samrj.devil.math;
    exports com.samrj.devil.math.topo;
    exports com.samrj.devil.math.zorder;
    exports com.samrj.devil.model;
    exports com.samrj.devil.model.constraint;
    exports com.samrj.devil.net;
    exports com.samrj.devil.phys;
    exports com.samrj.devil.util;
    exports com.samrj.devil.util.alloc;
}